from django.db import models

class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=False) 
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + " by " + self.author

class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)
    def __str__(self):
        return self.abbreviation
